﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApplication1.Entities.Entities.PostGre
{
    public partial class PostGreDbContext : DbContext
    {
        //public PostGreDbContext()
        //{
        //}

        public PostGreDbContext(DbContextOptions<PostGreDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Tbbarang> Tbbarang { get; set; }
        public virtual DbSet<Tbbenda> Tbbenda { get; set; }
        public virtual DbSet<Tbbuku> Tbbuku { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Server=localhost;Database=db_2;Username=postgres;Password=12345678;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tbbarang>(entity =>
            {
                entity.HasKey(e => e.Kodebarang)
                    .HasName("tbbarang_pkey");

                entity.Property(e => e.Kodebarang).IsFixedLength();
            });

            modelBuilder.Entity<Tbbenda>(entity =>
            {
                entity.HasKey(e => e.Kodebenda)
                    .HasName("tbbenda_pkey");

                entity.Property(e => e.Kodebenda).IsFixedLength();
            });

            modelBuilder.Entity<Tbbuku>(entity =>
            {
                entity.HasKey(e => e.Kodebuku)
                    .HasName("tbbuku_pkey");

                entity.Property(e => e.Kodebuku).IsFixedLength();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
